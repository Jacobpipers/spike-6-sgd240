# Spike Report

## SPIKE 6 - Packaging and Executable

### Introduction

We’ve now got a nearly complete, testable game.
We need to be able to distribute it to people who are not on our development team – so it’s time to package the game up into an Executable!
Our game currently targets PC, but we could use a similar process to learn how to deploy for Android, iOS, or a Console.

### Goals

Building on Core Spikes 4 and 5:
* Package your project into an Executable
* Document what options you selected, if any, and what effects they had on either the Packaging process or the Execution of the game (i.e. frame-rate differences, load-time, or packaging-time).



### Personnel

In this section, list the primary author of the spike report, as well as any personnel who assisted in completing the work.

* Primary - Jacob Pipers
* Secondary - N/A

### Technologies, Tools, and Resources used

Provide resource information for team members who wish to learn additional information from this spike.

* Visual Studio
* Unreal Engine


### Tasks Undertaken

Show only the key tasks that will help the developer/reader understand what is required.

1. Read through [Unreal Deployment Packaging](https://docs.unrealengine.com/latest/INT/Engine/Basics/Projects/Packaging/index.html)
2. Package worked for windows.

### What we found out




### [Optional] Open Issues/Risks

List out the issues and risks that you have been unable to resolve at the end of the spike. You may have uncovered a whole range of new risks as well.

### [Optional] Recommendations

You may state that another spike is required to resolve new issues identified (or) indicate that this spike has increased the team�s confidence in XYZ and move on.