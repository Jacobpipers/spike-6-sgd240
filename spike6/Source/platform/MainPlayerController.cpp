// Fill out your copyright notice in the Description page of Project Settings.

#include "platform.h"
#include "MainPlayerController.h"


void AMainPlayerController::BeginPlay()
{
	Super::BeginPlay();

	auto ControlledCharacter = AMainPlayerController::GetControlledCharacter();
	if (!ControlledCharacter)
	{
		UE_LOG(LogTemp, Warning, TEXT("Not Possessing Player"));
		ControlledCharacter = AMainPlayerController::GetControlledCharacter();
		//UGameplayStatics::CreatePlayer(, -1, true);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Possessing Player"));
	}
	
}

void AMainPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	if (ControlledCharacter)
	{
		int32 id = GetLocalPlayer()->GetControllerId();
		if (id == 0)
		{
			InputComponent->BindAxis("MoveForward", AMainPlayerController::GetControlledCharacter(), &APlatformCharacter::MoveForward);
			InputComponent->BindAction("Jump", IE_Pressed, AMainPlayerController::GetControlledCharacter(), &APlatformCharacter::OnStartJump);
			InputComponent->BindAction("Jump2", IE_Released, AMainPlayerController::GetControlledCharacter(), &APlatformCharacter::OnStartJump);
		}
		else if (id == 1)
		{
			InputComponent->BindAxis("MoveForward2", AMainPlayerController::GetControlledCharacter(), &APlatformCharacter::MoveForward);
			InputComponent->BindAction("Jump2", IE_Pressed, AMainPlayerController::GetControlledCharacter(), &APlatformCharacter::OnStartJump);
			InputComponent->BindAction("Jump2", IE_Released, AMainPlayerController::GetControlledCharacter(), &APlatformCharacter::OnStartJump);
		}
	}
	
	
	/*if (ControlledCharacter)
	{
		InputComponent->BindAxis("MoveForward", ControlledCharacter, &APlatformCharacter::MoveForward);
		InputComponent->BindAction("Jump", IE_Pressed, ControlledCharacter, &APlatformCharacter::OnStartJump);
		InputComponent->BindAction("Jump", IE_Released, ControlledCharacter, &APlatformCharacter::OnStartJump);
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, "Cast<APlayerCharacter>(GetControlledPawn()) is NULL for some reason");
		ControlledCharacter = AMainPlayerController::GetControlledCharacter();
	}*/
}

void AMainPlayerController::Tick(float DeltaTime)
{

	ControlledCharacter = AMainPlayerController::GetControlledCharacter();

	AMainPlayerController::SetupInputComponent();
}


APlatformCharacter* AMainPlayerController::GetControlledCharacter() const
{
	return Cast<APlatformCharacter>(GetControlledPawn());
}