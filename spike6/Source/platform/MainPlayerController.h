// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "PlatformCharacter.h"
#include "GameFramework/PlayerController.h"
#include "MainPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class PLATFORM_API AMainPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:

	// Called every frame
	virtual void Tick(float DeltaTime) override;


	APlatformCharacter* GetControlledCharacter() const;
	const APlatformCharacter* ControlledCharacter;
	// Called to bind functionality to input
	virtual void SetupInputComponent() override;


	virtual void BeginPlay() override;

};
